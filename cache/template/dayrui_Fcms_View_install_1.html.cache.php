<?php if ($fn_include = $this->_include("install/header.html")) include($fn_include); ?>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-refresh font-green"></i>
            <span class="caption-subject font-green">环境监测</span>
        </div>
    </div>
    <div class="portlet-body ">
        <form action="#" class="form-horizontal form-bordered ">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">PHP版本</label>
                    <div class="col-md-9">
                        <p class="form-control-static"> <?php echo PHP_VERSION; ?> </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">PHP版本要求</label>
                    <div class="col-md-9">
                        <p class="form-control-static"> 7.3及以上版本 </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">MySQL版本要求</label>
                    <div class="col-md-9">
                        <p class="form-control-static"> 5.6及以上版本 </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">目录读写权限</label>
                    <div class="col-md-9" style="margin-top:-1px;">
                        <?php if (is_array($path)) { $key_code=-1;$count_code=dr_count($path);foreach ($path as $name=>$code) { $key_code++; $is_first=$key_code==0 ? 1 : 0;$is_last=$count_code==$key_code+1 ? 1 : 0; ?>
                        <p style="margin:10px 0;<?php if (!$code) { ?>color:red<?php } ?>"> <?php if (!$code) { ?>[错误]<?php } else { ?>[正确]<?php } ?>&nbsp;&nbsp;<?php echo $name; ?> </p>
                        <?php } } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">PHP环境检测</label>
                    <div class="col-md-9" style="margin-top:-1px;">
                        <?php if (is_array($php)) { $key_t=-1;$count_t=dr_count($php);foreach ($php as $t) { $key_t++; $is_first=$key_t==0 ? 1 : 0;$is_last=$count_t==$key_t+1 ? 1 : 0; if (!$t['code']) { ?>
                        <p style="margin:10px 0;color:red"> <?php $error = 1;?>[不支持]&nbsp;&nbsp;<?php echo $t['name']; ?> &nbsp; &nbsp;<a href="<?php echo $t['help']; ?>" target="_blank">查看帮助</a></p>
                        <?php } else { ?>
                        <p style="margin:10px 0;"> [支持]&nbsp;&nbsp;<?php echo $t['name']; ?> </p>
                        <?php }  } } ?>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="portlet-title" style="min-height: 18px;">
    </div>
    <div class="portlet-body text-center">
        <?php if ($error) { ?>
        <button type="button" class="btn default"> 无法进行下一步安装 </button>
        <?php } else { ?>
        <a href="<?php echo $next_url; ?>" class="btn btn-success"> 下一步安装 </a>
        <?php } ?>
    </div>
</div>

<?php if ($fn_include = $this->_include("install/footer.html")) include($fn_include); ?>